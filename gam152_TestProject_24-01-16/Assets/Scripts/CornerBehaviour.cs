﻿using UnityEngine;
using System.Collections;

public class CornerBehaviour : MonoBehaviour {
    /// <summary>
    /// 
    /// 24/1/2016 V0.01 prototype
    /// NOT DONE YET - DO NOT USE THANK YOU
    /// 
    /// 
    /// -- Thanks.
    /// - Alex
    /// </summary>

    private PlayerBehaviour playerb;

    void Start()
    {
        playerb = gameObject.GetComponentInParent<PlayerBehaviour>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            playerb.grounded = true;
            Debug.Log("I'm on the ground!");
        }
		if (col.tag == "Ledge")
		{
			playerb.grounded = false;
			Debug.Log("Grabbing!");
		}
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            playerb.grounded = true;
        }

    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            playerb.grounded = false;
            Debug.Log("I'm off the ground!");
        }
    }
}
