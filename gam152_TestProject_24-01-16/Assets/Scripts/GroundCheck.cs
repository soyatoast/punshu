﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {
    /// <summary>
    /// 24/1/15 - v0.01 prototype
    /// This will CHECK IF THE PLAYER IS GROUNDED. 
    /// =============================
    /// SO WHAT YOU WILL NEED TO DO =
    /// =============================
    /// 1. CREATE A GAMEOBJECT AND MAKE IT A CHILD OF THE PLAYER
    /// 2. ATTACH THIS SCRIPT TO THE OBJECT 
    /// 3. MAKE A 2DCOLLIDER AT THE PLAYER'S FEET
    /// 4. SET THE 2DCOLLIDER Is Trigger ON.
    /// 
    /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /// BEFORE YOU START TESTING AND TELLING ME THERE IS A PROBLEM
    /// ATTACH THE PlayerBehaviour SCRIPT TO THE PARENT.
    /// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /// 
    ///     
    /// 
    /// ##############
    /// ERROR CHECKS #
    /// ##############
    /// CHECK IF 
    /// 1. THE GROUND IS TAGGED PROPERLY
    /// 2. THE STEPS ABOVE WERE IMPLEMENTED
    /// 3. CHECK STEP 1 AND 2 AGAIN.
    /// 4. MAKE SURE YOUR PREFABS HAVE THE CORRECT TAGS
    /// 
    /// 
    /// -- Thanks.
    /// - Alex
    /// </summary>

    private PlayerBehaviour playerb;

	void Start ()
    {
        playerb = gameObject.GetComponentInParent<PlayerBehaviour>();
	}
	
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            playerb.grounded = true;
            Debug.Log("I'm on the ground!");
        }
		if (col.tag == "Ledge")
		{
			playerb.grounded = false;
			Debug.Log("Grabbing!");
		}
    }
    
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            playerb.grounded = true;
        }
		if (col.tag == "Ledge")
		{
			playerb.grounded = false;
		}
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Ground")
        {
            playerb.grounded = false;
            Debug.Log("I'm off the ground!");
        }
		if (col.tag == "Ledge")
		{
			playerb.grounded = false;
			Debug.Log("Not Grabbing!");
		}
    }
}
