﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
    /// <summary>
    /// 24/1/16 - v0.01 prototype
    /// handles movement, animations, and booleans for hanging or grounded.
    /// 
    /// the player will be seperated into body and feet.
    /// the feet are a seperate collider.
    /// 
    /// refer to GroundCheck.cs for more information.
    /// please take note if I am not able to check stuff.
    /// 
    /// 
    /// -- Thanks.
    /// - Alex
    /// </summary>
    /// 
	/// <summary>
	/// 27/1/16 - v0.02 prototype
	/// handles movement, animations, and booleans for hanging or grounded.
	/// 
	/// Now with Ledgegrabbing!
	/// 
	/// -- Thanks.
	/// - Harry
	/// </summary>
	/// 

    // PARAMETERS FOR MOVEMENT VELOCITY
    public float MaxSpeed = 3;
    public float Speed = 50f;
    public float JumpHeight = 250f;


    // To use bools to check if the player is hanging at the edge or not. 
    // To determine how to do this in the near future. 
    public bool grounded = false;
	public bool hangingLeft = false;
	public bool hangingRight = false;
	public bool isHanging = false;

    private Rigidbody2D playerBody;
    private Animator playerAnim;
	private Collider2D ledgeCollider;

    // INITIALIZE RIGIDBODY AND ANIMATOR //
	void Start ()
    {
        playerBody = gameObject.GetComponent<Rigidbody2D>();
        playerAnim = gameObject.GetComponent<Animator>();
		ledgeCollider = gameObject.GetComponentInChildren<Collider2D>();
	}


    void Update()
    {
        // ANIMATIONS //
        playerAnim.SetBool("Grounded", grounded);
        playerAnim.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));

        // CHECK FOR ORIENTATION. IF IT IS FACING LEFT OR RIGHT FLIP THE X SCALE //
        if (Input.GetAxis("Horizontal") < -0.1f )
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        if (Input.GetButtonDown("Jump") && grounded)
        {
            playerBody.AddForce(Vector2.up * JumpHeight);
        }

    }

    void FixedUpdate ()
    {
        // HORIZONTAL MOVEMENT //
        float hForce = Input.GetAxis("Horizontal");

		LedgeHang();

        if (!isHanging) {
			playerBody.AddForce ((Vector2.right * Speed) * hForce);
		} 
		else ///hanging from the ledge
		{
			playerBody.constraints = RigidbodyConstraints2D.FreezePositionY;
			playerBody.gravityScale = 0;
		}
        if (playerBody.velocity.x > MaxSpeed)
        {
            playerBody.velocity = new Vector2(MaxSpeed, playerBody.velocity.y);
        }
        if (playerBody.velocity.x < -MaxSpeed) {
			playerBody.velocity = new Vector2 (-MaxSpeed, playerBody.velocity.y);
		} 
    }
	void LedgeHang()
		///
		/// 1. Check if player sprite is colliding with wall/ledge
		/// 2. If yes, Grab Ledge.
		/// 3. To jump from ledge, jump+opposite direction.
		/// 4. To get up to ledge, check if the space above is greater than the 
		///    height of the sprite. If yes, go up, if not, don't bother.
		///    
		/// - Harry
		///
		///
	{
		if(playerBody.IsTouching(ledgeCollider)) ///Should be touching ledge
		{
			isHanging = true;
			Debug.Log("I'm grabbing!");
		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Ledge")
		{
			isHanging = true;
			Debug.Log("Grabbing!");
		}
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Ledge")
		{
			isHanging = true;
		}
	}
	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "Ledge")
		{
			isHanging = false;
			Debug.Log("Not Grabbing!");
		}
	}
}
